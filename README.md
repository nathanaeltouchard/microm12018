# MicroM12018

Projet M1 Micro

## Configuration (First launch only)

Go to http://localhost:9080
Create a realm by importing realm-export.json file located in 'keycloak/'<br/>
Optional : create an admin user in the Micro-test realm

## Usage 

Start the application

```sh
 ./launch_micro.sh
```

Send a workflow quickly (for test)

```sh
 ./send_workflow.sh
```

lien pour la documentation : https://drive.google.com/open?id=1HS9xPS6Tek9cswVgQLYYzIMDkwxEqDOY

lien pour le Trello : https://trello.com/micromaster2019/home

Integration continue : Victor

Chef de projet : Etienne 

Groupe 1 :

    - Objectif : WES/TES
    - Membres : Emerick, Axel, Nicolas
    - Sous-chef : Nicolas
    - Trello : https://trello.com/b/FxAVp0Ev/wes-tes

Groupe 2 :

    - Objectif : Load balancing
    - Membres : Nathanaël, Pierre, Alexis
    - Sous-chef : Nathanaël
    - Trello : https://trello.com/b/PoJon3Kr/balancing

Groupe 3 : 

    - Objectif : Discovery
    - Membres : Victor, Nathan
    - Sous-chef : Nathan
    - Trello : https://trello.com/b/p1vIziD4/discovery

Groupe 4 :

    - Objectif : Security
    - Membres : Moussa, Etienne, Hugues
    - Sous-chef : Hugues
    - Trello : https://trello.com/b/GcIBm6Rq/keycloak