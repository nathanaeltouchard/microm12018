23 janvier 2019
1ère reunion

WES/TES:
	Squelette de WES Implémenté (avec spring boot)
	Stockage de fichier quasiment terminé
	TES en cours de documentation

Discovery:
	A lancé des instances de consul
	Découverte de App registrator 
	Quasiment terminé

Load balancing:
	Comprendre la configuration de traefik
	Serveur traefik implémenté
	
Securité:
	Keycloak documentation  
	Travail sur jetons JWT (dépendances à ajouter sur chaque microservices)
	
Integration continue:
	Il faut juste lui fournir les tests qu'on lance à la main, ils seront automatisés	

Travail à faire en plus :
	- Faire le schéma du projet avec mermaid
	- Ajouter un schéma explicatif dans chaque partie + petit résumé de ce qui est fait afin de
	vulgariser le travail pour les autres membres du projet
	- Faire des démonstrators dans chaque partie 
