20/03/2019

Consul & Traefik

	- Configuration dynamique terminée.

WES/TES (& Keycloak)	

	- Service Discovery intégré. (On vois toutes les instances de TES et les WES correspondants.)
	- Load balancing en cours.
	- Profils OK. (Dev/Prod)

Interface

	- Formulaire de création de workflow
	- Regarde actuellement les requêtes à envoyer vers WES pour afficher tous les workflows - A voir après pour le faire en fonction du rôle des utilisateurs (admin ou user)

Rapport

	- Rédaction du Post mortem (rapport de gestion de projet) et du manuel d'utilisation en cours. 

Démo

	- Choix final des membres qui s'occuperont de la démonstration (Victor, Etienne et Emerick.)
	- Nouveaux cas de scénarios mais toujours de la théorie. Besoin de l'interface pour aller plus loin.